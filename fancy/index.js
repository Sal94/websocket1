// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

server.listen(port, () => {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(path.join(__dirname, 'public')));

io.set('transports', ['websocket']);

var userNumbers = {
  'COVID-19': 0,
  'RICARDO': 0,
  'MOTO-MOTO': 0,
  'HARAMBE': 0
}

io.on('connection', (socket) => {
  var addedUser = false;
  
  // when the client emits 'new message', this listens and executes
  socket.on('new message', (data) => {
    // we tell the client to execute 'new message'
    socket.to(socket.room).emit('new message', {
      username: socket.username,
      message: data
    });
  });

  socket.on('add user', (sockInfo) => {
    if (addedUser) return;
    // we store the username in the socket session for this client
    socket.username = sockInfo.username;
    socket.room = sockInfo.room;
    
    console.log('HEREEE', sockInfo)
    ++userNumbers[socket.room];
    addedUser = true;
    
    socket.join(socket.room);
    socket.emit('login', {
      numUsers: userNumbers[socket.room]
    });
   
    socket.to(socket.room).emit('user joined', {
      username: socket.username,
      numUsers: userNumbers[socket.room]
    });
  });
  

  
  // when the client emits 'add user', this listens and executes

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', () => {
    socket.to(socket.room).emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', () => {
    socket.to(socket.room).emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', () => {
    if (addedUser) {
      --userNumbers[socket.room];
      // echo globally that this client has left
      socket.to(socket.room).emit('user left', {
        username: socket.username,
        numUsers: userNumbers[socket.room]
      });
    }
  });
});
  
